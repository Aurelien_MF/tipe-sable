import math

class vector():
    """
    a class of 2d vectors
    """
    def __init__(self, values):
        self.values = list(values)
    
    def unit_vector(self):#not in use (yet)
        l = abs(self)
        return vector(v/l for v in self.values)
    
    
    def rotate(self, angle):
        c = math.cos(angle)
        s = math.sin(angle)
        return vector((c*self[0]-s*self[1], s*self[0]+c*self[1]))

    def angle(self):
        if self.values[1]>=0:
            return math.acos(self.values[0]/abs(self))
        return -math.acos(self.values[0]/abs(self))

    #magic methods :
    def __iter__(self):#not in use (yet)
        for v in self.values:
            yield v
    def __pos__(self):
        return self
    def __neg__(self):
        return vector(-v for v in self.values)
    def __abs__(self):
        return math.sqrt(self.values[0]*self.values[0]+self.values[1]*self.values[1])
    def __str__(self):#can be used to debug
        return '('+str(self.values[0])+", "+str(self.values[1])+")"
    def __repr__(self):
        return 'vector'+self.__str__()
    def __getitem__(self, key):
        return self.values[key]
    def __setitem__(self, key, value):
        self.values[key] = value

    def __add__(self, other):
        return vector((self[0]+other[0], self[1]+other[1]))
    def __sub__(self, other):
        return vector((self[0]-other[0], self[1]-other[1]))
    def __mul__(self, other):
        if other.__class__ in (vector, tuple, list):
            return self[0]*other[0]+self[1]*other[1]
        return vector(v*other for v in self.values)
    def __div__(self, other):
        return vector(v/other for v in self.values)
    def __rmul__(self, other):
        return self.__mul__(other)
    def __mod__(self, other):
        return self-(self*other)*other/abs(other)**2

