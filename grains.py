from vector_math import vector
from PIL import Image, ImageDraw
import imageio, os

pi = 3.14159265358979323846264338327950
pi_2 = 2*pi

#shared characteristics of grains of sand
spring_T = .35
spring_N = .1
friction_coef = 0.70#tangential friction coefficient
dampening = .000015#normal (fluid) friction coefficient
cohesion = .001
max_radius = .00015
min_radius = .00005


gravity = vector((0,-9.81))


grain_colors = [(0,0,255),(0,128,255),(0,255,0),(128,255,0),(255,255,0),(255,128,0),(255,0,0),(128,0,0)]#depends on number of contacts

threshold = lambda t, x: (x>t)*x#a useful function

class Pile:
    """
    master class handling simulation
    """
    def __init__(self, ground_friction_coef, ground_spring, ground_dampening, width, height, surfaces = []):
        self.ground_friction_coef = ground_friction_coef
        self.ground_spring = ground_spring
        self.ground_dampening = ground_dampening
        self.width = width
        self.height = height
        self.grains = []
        self.contacts = []
        self.surfaces = [S+[(S[1]-S[0]).rotate(-pi/2).unit_vector(), (S[1]-S[0]).unit_vector()] for S in surfaces]#list of : [P1, P2, width] P1 and P2 vectors (orientation : if P1 on the left and P2 on the right, surface is on top) and P
        self.is_in_surface = [(lambda along, d2, S0, S3, depth:
                    lambda pos: (0<=(pos-S0)*S3<=depth and 0<=(pos-S0)*along<=d2))(S[1]-S[0], abs(S[1]-S[0])**2, S[0], S[3], S[2])
                    for S in self.surfaces]
        self.surface_depth = [(lambda S0,S3:
                (lambda pos: ((pos-S0)*S3)*S3))(S[0], S[3])
                for S in self.surfaces]#vector from surface to position
        #lambda functions may cause problems in this sort of case because they use the variables and not their values
        #check second answer on : https://stackoverflow.com/questions/6076270/python-lambda-function-in-list-comprehensions#34021333
        #separating grains to accelerate collision checking
        grid_bottom = -height*.1
        grid_top = height*1.1
        grid_right = width*1.1
        self.grid_height = int((grid_top-grid_bottom)/(2*max_radius))
        self.grid_width = int(grid_right/max_radius)
        self.box_height = (grid_top-grid_bottom)/self.grid_height
        self.box_width = 2*grid_right/self.grid_width
        self.grid_origin = vector((-grid_right-self.box_width, grid_bottom-self.box_height))
        self.get_grid_pos = lambda pos: (int((pos[0]-self.grid_origin[0])//self.box_width), int((pos[1]-self.grid_origin[1])//self.box_height))
        self.get_neighbours = lambda x,y: self.grid[x][y]+self.grid[x+1][y]+self.grid[x+1][y+1]+self.grid[x][y+1]+self.grid[x-1][y+1]+self.grid[x-1][y]+self.grid[x-1][y-1]+self.grid[x][y-1]+self.grid[x+1][y-1]
        self.grid = [[[] for _ in xrange(self.grid_height+2)] for _ in xrange(self.grid_width+2)]
        #grid of lists containing every grain in a specific cell on screen
        #as every cell's size is the diameter of grains, collisions should only be checked with grains in neighbouring cells.
        #buffer of always empty boxes all around the grid
    def update(self, dt):
        #update collisions
        for g in self.grains:
            x,y = self.get_grid_pos(g.position)
            for g2 in self.get_neighbours(x,y):
                if not(g2 in g.in_contact) and g2!=g and abs(g2.position-g.position)<g.radius+g2.radius:
                    g2.in_contact.append(g)
                    g.in_contact.append(g2)
                    self.contacts.append(Contact(g,g2))
        
        #initialize force at just gravity
        for g in self.grains:
            g.force = gravity*g.mass

        #compute collision forces and eventually remove obsolete contacts
        i=0
        while i < len(self.contacts):
            c = self.contacts[i]
            still_in_contact, force_12 = c.get_force()
            if still_in_contact:
                c.grain2.force += force_12
                c.grain1.force -= force_12
                i+=1
            else:
                self.contacts.pop(i)
                c.grain1.in_contact.remove(c.grain2)
                c.grain2.in_contact.remove(c.grain1)
        
        for g in self.grains:
            """
            if g.position[1]<0:#compute ground collision
                N_force = -g.position[1]*self.ground_spring - g.speed[1]*self.ground_dampening
                T_intensity = N_force*self.ground_friction_coef
                if g.speed[0]>(T_intensity-g.force[0])/g.mass*dt:
                    g.force+=vector((-T_intensity, N_force))
                elif g.speed[0]<(-T_intensity+g.force[0])/g.mass*dt:
                    g.force+=vector((T_intensity, N_force))
                else:
                    g.force+=vector((-g.speed[0]*g.mass/dt, N_force))#no-slip friction
            """
            if g.position[0]+g.radius > self.width:#right wall collision
                g.force+=vector(((self.width-g.position[0])*self.ground_spring -g.speed[0]*self.ground_dampening, 0))
            elif g.position[0]-g.radius < -self.width:#left wall collision
                g.force+=vector(((-self.width-g.position[0])*self.ground_spring -g.speed[0]*self.ground_dampening, 0))
            
            #compute collisions with surfaces
            for i in xrange(len(self.surfaces)):
                if self.is_in_surface[i](g.position):
                    S = self.surfaces[i]
                    N_force = -self.surface_depth[i](g.position)*self.ground_spring -(g.speed*S[3])*S[3]*self.ground_dampening
                    T_intensity = abs(N_force)*self.ground_friction_coef
                    if g.speed*S[4] > T_intensity - g.force*S[4]:
                        g.force += N_force - S[4]*T_intensity
                    elif g.speed*S[4] < -T_intensity + g.force*S[4]:
                        g.force += N_force + S[4]*T_intensity
                    else:
                        g.force += N_force - (g.speed*S[4]*g.mass/dt)*S[4]

            #Euler method and update of grain position on grid
            last_grid_pos = self.get_grid_pos(g.position)
            g.position += (g.speed + g.force/g.mass*dt/2)*dt
            g.position[1] %= self.height
            new_grid_pos = self.get_grid_pos(g.position)
            if new_grid_pos != last_grid_pos:#change grid place if necessary
                self.grid[last_grid_pos[0]][last_grid_pos[1]].remove(g)
                self.add_to_grid(new_grid_pos, g)
            g.speed += g.force/g.mass*dt
    def add_grain(self, position, mass, radius, force = False):
        position = vector(position)
        if not(force):
            for g in self.grains:
                if abs(position-g.position)<g.radius + radius:
                    return False
        self.grains.append(Grain(position, mass, radius))
        grid_pos = self.get_grid_pos(position)
        self.add_to_grid(grid_pos, self.grains[-1])
        return True
    def add_to_grid(self, grid_pos, grain):
        if grid_pos[0]>0 and grid_pos[1]>0 and grid_pos[0]<self.grid_width+1 and grid_pos[1]<self.grid_height+1:
            self.grid[grid_pos[0]][grid_pos[1]].append(grain)
            return
        #delete grain otherwise
        print "debug : deleting a grain (grid_pos =",grid_pos,")"
        self.grains.remove(grain)

class Grain:
    def __init__(self, position, mass, radius):
        self.position = position#vector
        self.speed = vector((0,0))
        self.mass = mass
        self.radius = radius
        self.in_contact = []
        self.color = (int(255*(radius-min_radius)/(max_radius-min_radius)), int(127*(radius-min_radius)/(max_radius-min_radius)), 0)

class Contact:
    """
    a contact is modelled by :
        -two springs (tangential and normal)
        -fluid friction in the normal direction
        -solid friction in the tangential direction*

    For the tangential component, it is important to store the angle of initial contact(and modify it in case it slips). Hence the use of a class for that.
    it should be noted that grains don't ever rotate.
    """
    def __init__(self, grain1, grain2):
        self.grain1, self.grain2 = grain1, grain2
        self.stick_vector = grain2.position-grain1.position
        self.stick_vector = self.stick_vector*((grain1.radius+grain2.radius)/abs(self.stick_vector))
        self.slipped = False#used for debug
        self.last_norm_force = 0#used for debug
        self.shear = 0#used for debug
        #it now has a norm of exactly R1+R2
    def get_force(self):
        stick_distance = cohesion*(self.grain1.radius+self.grain2.radius)#for cohesion
        self.slipped = False
        new_vector = self.grain2.position-self.grain1.position#1 to 2
        new_vect_angle = new_vector.angle()
        depth = -abs(new_vector)+self.grain1.radius+self.grain2.radius
        if depth < -stick_distance:
            return False, vector((0,0))
        angle = (new_vector.angle()-self.stick_vector.angle()+pi)%pi_2-pi#from -pi to pi
        relative_speed = self.grain2.speed-self.grain1.speed#2 relative to 1
        N_speed, T_speed = tuple(relative_speed.rotate(-new_vect_angle))
        normal_intensity = depth*spring_N-N_speed*dampening#1 on 2
        tangential_intensity = -angle*(self.grain1.radius+self.grain2.radius)/2.*spring_T
        max_tangential = max(0,normal_intensity)*friction_coef
        if abs(tangential_intensity) > max_tangential:
            self.slipped = True
            if angle > 0:
                correction_angle = angle - max_tangential/(self.grain1.radius+self.grain2.radius)/2./spring_T
                tangential_intensity = -max_tangential
            else:
                correction_angle = angle + max_tangential/(self.grain1.radius+self.grain2.radius)/2./spring_T
                tangential_intensity = max_tangential
            self.stick_vector = self.stick_vector.rotate(correction_angle)
            #TODO : if I use angle afterward, it should be corrected :
            #   angle -= correction_angle
            #   plus check if every variable still makes sense
        force = vector((normal_intensity, tangential_intensity)).rotate(new_vect_angle)
        self.last_norm_force = normal_intensity
        try:
            self.shear = abs(tangential_intensity)/max_tangential
        except ZeroDivisionError:
            self.shear = 1.
        return True, force#force exerted by 1 on 2

def generate_img(environment, size, center, ratio=.01):
    #ratio: size of a pixel in meters
    #size = (width, height) in pixels
    ratio = float(ratio)
    def pos_to_coord(pos):
        temp = (pos-center)/ratio
        temp[1] = -temp[1]
        return temp+size/2.
    img = Image.new('RGB',tuple(size),(255,255,255))
    draw = ImageDraw.Draw(img)
    draw.rectangle(((environment.width-center[0])/ratio+size[0]/2., 0, size[0], size[1]), fill = (0,0,0))#draw right wall
    draw.rectangle(((-environment.width-center[0])/ratio+size[0]/2., 0, 0, size[1]), fill = (0,0,0))#draw left wall
    for S in environment.surfaces:
        draw.polygon([tuple(pos_to_coord(S[0])),tuple(pos_to_coord(S[1])),tuple(pos_to_coord(S[1]+S[3]*S[2])),tuple(pos_to_coord(S[0]+S[3]*S[2]))], fill = (0,255,0))
    for g in environment.grains:
        pos = (g.position-center)/ratio
        pos = (pos[0]+size[0]/2., -pos[1]+size[1]/2.)
        r = g.radius/ratio
        try:
            color = grain_colors[len(g.in_contact)]
        except IndexError:
            color = grain_colors[-1]
        color = g.color
        draw.ellipse((pos[0]-r, pos[1]-r, pos[0]+r, pos[1]+r), fill = color)
    for c in environment.contacts:
        pos1 = (c.grain1.position-center)/ratio
        pos2 = (c.grain2.position-center)/ratio
        color = (0,0,int(255*c.shear))#shear based
        color = (0,0,0)
        draw.line((pos1[0]+size[0]/2., -pos1[1]+size[1]/2., pos2[0]+size[0]/2., -pos2[1]+size[1]/2.), fill = color)
    del draw
    return img

def generate_img_analysis(environment, size, center, ratio=.01):
    #ratio: size of a pixel in meters
    #size = (width, height) in pixels
    ratio = float(ratio)
    def pos_to_coord(pos):
        temp = (pos-center)/ratio
        temp[1] = -temp[1]
        return temp+size/2.
    img = Image.new('RGB',tuple(size),(255,255,255))
    draw = ImageDraw.Draw(img)
    draw.rectangle(((environment.width-center[0])/ratio+size[0]/2., 0, size[0], size[1]), fill = (0,0,0))#draw right wall
    draw.rectangle(((-environment.width-center[0])/ratio+size[0]/2., 0, 0, size[1]), fill = (0,0,0))#draw left wall
    for S in environment.surfaces:
        draw.polygon([tuple(pos_to_coord(S[0])),tuple(pos_to_coord(S[1])),tuple(pos_to_coord(S[1]+S[3]*S[2])),tuple(pos_to_coord(S[0]+S[3]*S[2]))], fill = (0,0,255))
    del draw
    return img

def update_img_analysis(img, environment, size, center, ratio=0.1):
    draw = ImageDraw.Draw(img)
    max_speed = 0
    for g in environment.grains:
        pos = (g.position-center)/ratio
        pos = (pos[0]+size[0]/2., -pos[1]+size[1]/2.)
        #r = g.radius/ratio
        r = 2
        speed = abs(g.speed)*1500
        max_speed = max(speed, max_speed)
        color = (255-int(speed), int(speed), 0)
        color = tuple(min(255, max(0,c)) for c in color)
        draw.ellipse((pos[0]-r, pos[1]-r, pos[0]+r, pos[1]+r), fill = color)
    print "max_speed =",max_speed
    del draw
    return img



if __name__ == "__main__":
    import sys, fcntl, os
    fcntl.fcntl(0, fcntl.F_SETFL, os.O_NONBLOCK)
    try:
        print "creating world"
        aperture = .0020
        width = .005
        height = .01
        base = .001
        angle = pi/4
        from math import tan
        #friction, spring, dampening, width/2, height, surfaces
        world = Pile(.8,.02,.000015, width/2,height, [[vector((aperture/2,base)), vector((width/2+.01,base+tan(angle)*(width+.01-aperture)/2)), .002], [vector((-width/2-.01,base+tan(angle)*(width+.01-aperture)/2)), vector((-aperture/2,base)), .002]])
        dt = .00003
        t = 0
        simulation_time = 10.00
        import random
        framerate = 555
        frame_n = 0
        file_name = lambda n: "img_"+str(n)+".png"
        #size of initial pile
        w = width*.9
        h = .010
        print "adding grains"
        for _ in xrange(4000):
            x = (random.random()-.5)*w
            y = random.random()*h
            r = random.random()*(max_radius-min_radius)+min_radius
            #position, mass, radius
            world.add_grain((x,y), r**2, r)
        result = False
        import time
        analysis_img = generate_img_analysis(world, vector((550,800)), vector((0,.005)), .0000125)
        print "simulating"
        average_time = 0
        delay = 0
        for _ in xrange(int(simulation_time/dt)):
            loop_start = time.time()
            if t<=simulation_time/3. and len(world.grains)<1000:
                for _ in xrange(15):
                    y = random.random()*h
                    x = (random.random()-.5)*w
                    r = random.random()*(max_radius-min_radius)+min_radius
                    #position, mass, radius
                    world.add_grain((x,y), r**2, r)
            world.update(dt)
            t+=dt
            if t>= (frame_n+1.)/framerate:
                last_frame = t
                frame_n += 1
                print "creating frame",frame_n
                if frame_n == 40:
                    analysis_img = generate_img_analysis(world, vector((550,800)), vector((0,.005)), .0000125)
                generate_img(world, vector((550,800)), vector((0,.005)), .0000125).save(file_name(frame_n), "PNG")
                analysis_img = update_img_analysis(analysis_img, world, vector((550,800)), vector((0,.005)), .0000125)
                analysis_img.save("analysis_"+str(frame_n)+".png", "PNG")
                try:
                    stdin = sys.stdin.readline()
                except:
                    pass
                else:
                    try:
                        delay = int(stdin.strip())
                    except:
                        pass
            time.sleep(delay*.001)
            average_time *= .95
            average_time += .05*(time.time()-loop_start)
            time_left = (simulation_time-t)/dt*average_time
            print "\033[42m"*result+str(t)+"\033[0m\t",len(world.grains), len(world.contacts), str(int(time_left//3600))+"h"+str(int(time_left//60%60))+"m"+str(int(time_left%60)), "  \t", delay, str(int((1-(delay*.001/average_time))*100))+"%"
    except KeyboardInterrupt:
        fcntl.fcntl(0, fcntl.F_SETFL, os.O_RDONLY)
        if not(raw_input("generate gif ?").lower() in ['','y','yes']):
            quit()

    #generate gif
    print "generating gif ..."
    with imageio.get_writer("out.gif", mode="I") as writer:
        for name in [file_name(n+1) for n in xrange(frame_n)]:
            writer.append_data(imageio.imread(name))
            #os.remove(name)
